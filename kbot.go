package main

import (
	"context"
	"fmt"
	"html"
	"log"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
	"k8s.io/api/events/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
)

type touser string

func (t touser) Recipient() string {
	return string(t)
}

func tbot(cli *kubernetes.Clientset, cfg *Config) { //, token string, adminId string) {
	admin := cfg.admin //touser(adminId)

	b, err := tb.NewBot(tb.Settings{
		Token:  cfg.token,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	b.Handle("/ver", func(m *tb.Message) {
		b.Send(m.Sender, fmt.Sprintf("v: %s", GitCommit))
	})

	b.Handle("/ext", func(m *tb.Message) {
		// if strconv.Itoa(m.Sender.ID) == adminId {
		if cfg.IsUser(m.Sender.ID) {
			ret, err := extingress(cli)
			if err != nil {
				b.Send(m.Sender, err.Error())
			} else {
				b.Send(m.Sender, ret)
			}
		}
	})

	sendAll := func(m string) {
		b.Send(admin, m, &tb.SendOptions{ParseMode: tb.ModeHTML})
		for _, u := range cfg.users {
			b.Send(u, m, &tb.SendOptions{ParseMode: tb.ModeHTML})
		}
	}

	go func() {
		_, err := cli.EventsV1beta1().Events("").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Println("List events error " + err.Error())
			return
		}

		sendAll(fmt.Sprintf("<b>Launched %s</b>", GitCommit))

		w, err := cli.EventsV1beta1().Events("").Watch(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Println("Watch error " + err.Error())
			return
		}
		for {
			e := <-w.ResultChan()
			if e.Object != nil {
				vv := e.Object.DeepCopyObject().(*v1beta1.Event)
				if vv.Reason != "Pulled" && vv.Reason != "Scheduled" && vv.Reason != "Pulling" && vv.Reason != "Killing" && vv.Reason != "Created" && vv.Reason != "Started" {
					if time.Now().Add(-time.Minute * 5).After(vv.CreationTimestamp.Time) {
						sendAll(fmt.Sprintf("%s <b>%s</b>\n[%s]\n%s\n%s\n<pre>%s</pre>", typeIcon(e.Type), vv.Reason, vv.Namespace+"/"+vv.GetName(), vv.CreationTimestamp.Format(time.RFC3339), time.Now().Add(-time.Minute*5).Format(time.RFC3339), html.EscapeString(vv.Note)))
						time.Sleep(150 * time.Millisecond)
					}
				}
			} else {
				log.Println("trace: event Object nil ", e.Type)
			}
		}
	}()

	b.Start()
}

func typeIcon(tp watch.EventType) string {
	switch tp {
	case watch.Added:
		return "➕"
	case watch.Modified:
		return "✏️"
	case watch.Deleted:
		return "➖"
	default:
		return string(tp)
	}
}
