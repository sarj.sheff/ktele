package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"path/filepath"
	"strconv"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

type Point struct {
	IP        string
	Port      int32
	Namespace string
	Name      string
}

type Config struct {
	admin touser
	users []touser
	token string // bot token
}

func (cfg *Config) IsUser(id int) bool {
	u := strconv.Itoa(id)
	if u == string(cfg.admin) {
		return true
	} else {
		for _, v := range cfg.users {
			if string(v) == u {
				return true
			}
		}
	}
	return false
}

var kubeconfig *string
var GitCommit string

func main() {
	log.Println(GitCommit)
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}

	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		log.Println("Config error " + err.Error())
		config, err = rest.InClusterConfig()
		if err != nil {
			log.Println("InCluster config error " + err.Error())
			return
		}
	}

	cli, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	sss, err := cli.CoreV1().Secrets("default").Get(context.TODO(), "kube-tele", metav1.GetOptions{})
	if err != nil {
		log.Println("Set token and adminID in secret default/kube-tele")
		return
	}
	cfg := Config{admin: touser(string(sss.Data["adminID"])), token: string(sss.Data["token"])}
	for k, v := range sss.Data {
		if strings.HasPrefix(k, "user-") {
			cfg.users = append(cfg.users, touser(string(v)))
		}
	}

	tbot(cli, &cfg)
}

func extingress(cli *kubernetes.Clientset) (string, error) {
	stat := map[string][]Point{}
	ret := ""

	nodes, err := cli.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	// Собираем внешние ip адреса
	for _, i := range nodes.Items {
		for _, ip := range i.Status.Addresses {
			stat[ip.Address] = []Point{}
		}
	}

	// Обходим все endpointы
	ends, err := cli.CoreV1().Endpoints("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	for _, e := range ends.Items {
		for _, ee := range e.Subsets {
			for _, adr := range ee.Addresses {
				for _, p := range ee.Ports {
					pp := Point{adr.IP, p.Port, e.Namespace, e.Name}
					if val, ok := stat[adr.IP]; ok {
						stat[adr.IP] = append(val, pp)
					}
				}
			}
		}
	}

	// Обходим все servicы
	svc, err := cli.CoreV1().Services("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return "", err
	}

	for _, e := range svc.Items {
		if len(e.Spec.ExternalIPs) > 0 {
			for _, ip := range e.Spec.ExternalIPs {
				for _, p := range e.Spec.Ports {
					pp := Point{ip, p.Port, e.Namespace, e.Name}
					if val, ok := stat[ip]; ok {
						stat[ip] = append(val, pp)
					}
				}
			}
		}
	}

	for ip, ss := range stat {
		for _, pp := range ss {
			ret += fmt.Sprintf("%s:%d  %s/%s\n", ip, pp.Port, pp.Namespace, pp.Name)
		}
	}
	return ret, err
}
