FROM alpine

# kubectl create clusterrolebinding default-view1 --clusterrole=cluster-admin --serviceaccount=default:default

ADD kube-tele /app/

WORKDIR /app

RUN chmod 777 /app/*

CMD ["/app/kube-tele"]