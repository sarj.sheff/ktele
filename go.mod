module sheff.online/kube-tele

go 1.16

require (
	gopkg.in/tucnak/telebot.v2 v2.4.1
	k8s.io/api v0.22.3
	k8s.io/apimachinery v0.22.3
	k8s.io/client-go v0.22.3
)
